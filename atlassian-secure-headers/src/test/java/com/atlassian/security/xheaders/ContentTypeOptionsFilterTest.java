package com.atlassian.security.xheaders;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ContentTypeOptionsFilterTest {

    private ContentTypeOptionsFilter filter;

    @Mock
    private FilterConfig config;
    @Mock
    private HttpServletRequest req;
    @Mock
    private HttpServletResponse res;
    @Mock
    private FilterChain mockChain;

    @Before
    public void setUp() throws Exception {
        filter = new ContentTypeOptionsFilter();
    }

    @Test
    public void doFilter() throws Exception {
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res).setHeader(eq("X-Content-Type-Options"), eq("nosniff"));
    }

    @Test
    public void skipFilter() throws Exception {
        when(req.getAttribute("skip!X-Content-Type-Options")).thenReturn(Boolean.TRUE);
        filter.init(config);
        filter.doFilter(req, res, mockChain);
        verify(res, never()).setHeader(eq("X-Content-Type-Options"), any());
    }
}