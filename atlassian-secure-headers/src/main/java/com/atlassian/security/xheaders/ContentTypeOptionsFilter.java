package com.atlassian.security.xheaders;

import com.atlassian.security.ResponseHeaderFilter;

import javax.servlet.annotation.WebFilter;

/**
 * A simple filter that adds the <code>X-Content-Type-Options: nosniff</code> header, which prevents certain browsers
 * from incorrectly guessing MIME-types (which in turn prevents some cross-site attacks).
 * <p>
 * Note that the header can be skipped by setting a request attribute of <code>skip!X-Content-Type-Options</code>.
 */
@WebFilter(asyncSupported = true)
public class ContentTypeOptionsFilter implements ResponseHeaderFilter {

    @Override
    public String getHeaderName() {
        return "X-Content-Type-Options";
    }

    @Override
    public String getHeaderFields() {
        return "nosniff";
    }

}
