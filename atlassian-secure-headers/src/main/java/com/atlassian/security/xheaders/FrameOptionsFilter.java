package com.atlassian.security.xheaders;

import com.atlassian.security.ResponseHeaderFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import java.util.Optional;

/**
 * Adds the `X-Frame-Options` response header to all responses, with the following possible values:
 * <ul>
 *     <li><code>Deny</code> (default) - don't allow pages from this site/service to be framed</li>
 *     <li><code>SameOrigin</code> - allow pages from the same source to frame pages from this site/service</li>
 *     <li><code>Allow-from http://example.com</code> - allow pages from this site/service to be framed by pages from example.com</li>
 * </ul>
 * <p>
 * The header field value is controlled by a servlet init parameter, containing either the string "SameOrigin" or the
 * URL that should be appended to "Allow-from".
 * <p>
 * Note that the header can be skipped by setting a request attribute of <code>skip!X-Frame-Options</code>.
 * <p>
 * {@code
 *     	<init-param>
 *     	    <param-name>x-frame-options</param-name>
 *     	    <param-value>https://atlassian.com</param-value>
 *     	</init-param>
 * }
 * <p>
 * Generates <code>X-Frame-Options: Allow-from https://atlassian.com</code>
 * <p>
 * {@code
 *     	<init-param>
 *     	    <param-name>x-frame-options</param-name>
 *     	    <param-value>SameOrigin</param-value>
 *     	</init-param>
 * }
 * <p>
 * Generates <code>X-Frame-Options: SameOrigin</code>
 * <p>
 * Note that this header is considered deprecated but is supported by Internet Explorer 11, which does not support the
 * Content Security Policy Level 2 header.
 */
@WebFilter(asyncSupported = true)
public class FrameOptionsFilter implements ResponseHeaderFilter {

    private static String allowFrom;

    @Override
    public String getHeaderName() {
        return "X-Frame-Options";
    }

    @Override
    public String getHeaderFields() {
        return allowFrom;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        allowFrom = Optional.ofNullable(filterConfig.getInitParameter("x-frame-options")).orElse("Deny");
        if (!"SameOrigin".equals(allowFrom) && !"Deny".equals(allowFrom)) {
            allowFrom = "Allow-from " + allowFrom;
        }
    }

}
