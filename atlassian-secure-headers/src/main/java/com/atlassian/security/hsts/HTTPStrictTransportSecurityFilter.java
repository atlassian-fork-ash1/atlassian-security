package com.atlassian.security.hsts;

import com.atlassian.security.ResponseHeaderFilter;

import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.stream.Stream;

/**
 * A filter to add the <code>Strict-Transport-Security</code> response header, which indicates to the client that the
 * service/site <strong>MUST</strong> be accessed over HTTPS until the HSTS <code>max-age</code> field value is reached,
 * which we default to one year. Note that the header can be skipped by setting a request attribute of
 * <code>skip!Strict-Transport-Security</code>.
 * <p>
 * Two other fields can be added via init parameters: "includeSubdomains" and "preload".
 * <p>
 * {@code
 *     	<init-param>
 *     	    <param-name>includeSubdomains</param-name>
 *     	    <param-value>true</param-value> <!-- "true" is case-insensitive -->
 *     	</init-param>
 *     	<init-param>
 *     	    <param-name>preload</param-name>
 *     	    <param-value>TRUE</param-value>
 *     	</init-param>
 * }
 */
@WebFilter(asyncSupported = true)
public class HTTPStrictTransportSecurityFilter implements ResponseHeaderFilter {

    private static final String INCLUDE_SUB_DOMAINS = "includeSubDomains";
    private static final String PRELOAD = "preload";

    private static String hstsHeaderFields;

    @Override
    public String getHeaderName() {
        return "Strict-Transport-Security";
    }

    @Override
    public String getHeaderFields() {
        return hstsHeaderFields;
    }

    private boolean isSet(FilterConfig filterConfig, String param) {
        return "true".equalsIgnoreCase(filterConfig.getInitParameter(param));
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        StringJoiner fields = new StringJoiner(";")
                .add("max-age=" + Optional.ofNullable(filterConfig.getInitParameter("max-age")).orElse("31536000"));

        Stream.of(INCLUDE_SUB_DOMAINS, PRELOAD)
                .filter(s -> isSet(filterConfig, s))
                .forEach(fields::add);

        hstsHeaderFields = fields.toString();
    }
}
