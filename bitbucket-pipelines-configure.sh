#!/bin/bash
mvn --version

MAVEN_SETTINGS="$(mvn --version | sed -n -E 's/Maven home: (.*)/\1/p')/conf/settings.xml"

if [ ! -f "$MAVEN_SETTINGS" ]; then
       echo "Maven not installed, or maven settings is in a different place on this docker image! Not found at '$MAVEN_SETTINGS'"
          exit 1
      fi

      echo "Updating configuration in Maven settings file: '$MAVEN_SETTINGS'"

      sed -i'back' '/<servers>/ a\
          <server><id>atlassian-private</id><username>${atlassian_private_username}</username><password>${atlassian_private_password}</password></server>' "$MAVEN_SETTINGS"

      sed -i'bak' '/<profiles>/ a\
          <profile><id>atlassian-private</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-private</id><name>Atlassian Private</name><url>https://maven.atlassian.com/content/repositories/atlassian-private/</url><layout>default</layout></repository></repositories><pluginRepositories><pluginRepository><id>atlassian-private</id><url>https://maven.atlassian.com/content/groups/internal</url></pluginRepository></pluginRepositories></profile>' "$MAVEN_SETTINGS"

      sed -i'bak' '/<profiles>/ a\
          <profile><id>atlassian-public</id><activation><activeByDefault>true</activeByDefault></activation><repositories><repository><id>atlassian-public</id><url>https://maven.atlassian.com/repository/public</url></repository></repositories></profile>' "$MAVEN_SETTINGS"
