package com.atlassian.security.xml.libs;

import com.atlassian.security.xml.SecureXmlParserFactory;
import org.jdom.input.SAXBuilder;
import org.xml.sax.XMLReader;

/**
 * A class with a utility method to produce a <a href='http://www.jdom.org/'>JDOM</a> parser suitable for untrusted XML.
 *
 * @since 3.0
 */
public class SecureJdomFactory
{
    private SecureJdomFactory()
    {
    }

    /**
     * Create a JDOM {@link SAXBuilder} using {@link SecureXmlParserFactory}, suitable for parsing XML from an untrusted source.
     */
    public static SAXBuilder newSaxBuilder()
    {
        return new SAXBuilder()
        {
            @Override
            protected XMLReader createParser()
            {
                return SecureXmlParserFactory.newNamespaceAwareXmlReader();
            }
        };
    }
}
