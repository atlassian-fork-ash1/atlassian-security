package com.atlassian.security.xml;

import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;

import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;

/**
 * SAXParserFactory which does not allow certain features to be set, delegates down to the usual SAXParserFactory
 *
 * @since 3.1.3
 */

class RestrictedSAXParserFactory extends SAXParserFactory
{

    private final SAXParserFactory delegate;

    RestrictedSAXParserFactory(SAXParserFactory inner)
    {
        delegate = inner;
    }

    @Override
    public SAXParser newSAXParser() throws ParserConfigurationException, SAXException
    {
        final SAXParser innerParser = delegate.newSAXParser();
        return new RestrictedSAXParser(innerParser);
    }

    @Override
    public void setNamespaceAware(boolean awareness)
    {
        delegate.setNamespaceAware(awareness);
    }

    @Override
    public void setValidating(boolean validating)
    {
        delegate.setValidating(validating);
    }

    @Override
    public boolean isNamespaceAware()
    {
        return delegate.isNamespaceAware();
    }

    @Override
    public boolean isValidating()
    {
        return delegate.isValidating();
    }

    @Override
    public void setFeature(String name, boolean value) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException
    {
        if (checkFeatures(name, value))
        {
            delegate.setFeature(name, value);
        }
    }

    static boolean checkFeatures(String name, boolean value)
    {
        if (name.equals(FEATURE_SECURE_PROCESSING) && !value) {
           return false;
        }
        if (name.equals(SecureXmlParserFactory.ATTRIBUTE_LOAD_EXTERNAL) && value) {
            return false;
        }

        if (name.equals(SecureXmlParserFactory.FEATURE_EXTERNAL_GENERAL_ENTITIES) && value) {
            return false;
        }
        if (name.equals(SecureXmlParserFactory.FEATURE_EXTERNAL_PARAMETER_ENTITIES) && value) {
            return false;
        }
        return true;
    }

    @Override
    public boolean getFeature(String name) throws ParserConfigurationException, SAXNotRecognizedException, SAXNotSupportedException
    {
        return delegate.getFeature(name);
    }

    @Override
    public Schema getSchema()
    {
        return delegate.getSchema();
    }

    @Override
    public void setSchema(Schema schema)
    {
        delegate.setSchema(schema);
    }

    @Override
    public void setXIncludeAware(boolean state)
    {
        delegate.setXIncludeAware(state);
    }

    @Override
    public boolean isXIncludeAware()
    {
        return delegate.isXIncludeAware();
    }

}
