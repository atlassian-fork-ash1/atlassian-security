package com.atlassian.security.xml.libs;

import com.atlassian.security.xml.SecureXmlParserFactory;

import org.apache.commons.digester.Digester;

/**
 * A class with a utility method to produce a <a href='http://commons.apache.org/digester/'>Commons Digester</a> parser suitable for untrusted XML.
 *
 * @since 3.0
 */
public class SecureDigesterFactory
{
    private SecureDigesterFactory()
    {
    }

    /**
     * Create a new {@link Digester} using {@link SecureXmlParserFactory}, suitable for parsing XML from an untrusted source.
     */
    public static Digester newDigester()
    {
        final Digester digester = new Digester(SecureXmlParserFactory.newXmlReader());
        return digester;
    }
}
