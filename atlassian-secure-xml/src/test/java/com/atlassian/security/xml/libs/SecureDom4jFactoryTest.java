package com.atlassian.security.xml.libs;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import com.atlassian.security.xml.HttpAttemptDetector;
import com.atlassian.security.xml.SampleXmlDocuments;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.junit.Test;
import org.xml.sax.SAXParseException;

import static com.atlassian.security.xml.libs.SecureDom4jFactory.newSaxReader;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class SecureDom4jFactoryTest
{
    @Test
    public void testDom4jParserIsBroken() throws IOException, DocumentException
    {
        final InputStream in = this.getClass().getResourceAsStream("/evil.xml");
        assertNotNull("Could not load /evil.xml as stream.", in);


        SAXReader xmlReader = new SAXReader();
        org.dom4j.Document document = xmlReader.read(in);

        assertTrue("Did not load SYSTEM entity containing TOP SECRET", document.getRootElement().getText().contains("TOP SECRET"));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = newSaxReader().read(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotIncludedInResult() throws Exception
    {
        Document d = newSaxReader().read(new StringReader(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        newSaxReader().read(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test(timeout = 5000)
    public void testDom4jParserDoesNotEvaluateTooMuch() throws Exception
    {
        SAXReader saxReader = newSaxReader();

        try
        {
            saxReader.read(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
        }
        catch (DocumentException e)
        {
            assertTrue(e.getNestedException() instanceof SAXParseException);
        }
    }

    @Test
    public void testDom4jParserDoesNotReadExternalDtds() throws Exception
    {
        SAXReader xmlReader = newSaxReader();

        final HttpAttemptDetector httpAttemptDetector = new HttpAttemptDetector();
        new Thread(httpAttemptDetector).start();

        final String s = SampleXmlDocuments.externalUrlDtd(httpAttemptDetector.getUrl());
        Document document = xmlReader.read(new StringReader(s));
        assertEquals("root", document.getRootElement().getName());
        assertFalse("I don't want to see HTTP connection attempts", httpAttemptDetector.wasAttempted());
    }
}
