package com.atlassian.security.xml;
import com.atlassian.security.xml.SecureXmlParserFactory;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class JaxpParserBehaviourTest
{
    static DocumentBuilder createDocumentBuilder() throws ParserConfigurationException
    {
        return SecureXmlParserFactory.newDocumentBuilder();
    }

    @Test(expected = IllegalArgumentException.class)
    public void askingForUnknownAttributesFails()
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setAttribute("no-attribute-with-this-identifier", false);
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT)));
        Node n = d.getDocumentElement().getChildNodes().item(0);
        assertEquals("&", n.getTextContent());
    }

    @Test(expected = SAXParseException.class, timeout = 5000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.BILLION_LAUGHS)));
    }

    @Test
    public void externalEntityIsNotIncludedInDom() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.externalResourceEntity())));
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        createDocumentBuilder().parse(new InputSource(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl()))));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void externalParameterEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        try
        {
            createDocumentBuilder().parse(new InputSource(
                    new StringReader(SampleXmlDocuments.externalParameterEntity(detector.getUrl()))));
        }
        catch (SAXParseException spe)
        {
            // Don't care
        }
        catch (IOException e)
        {
            // Don't care
        }

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToFile() throws Exception
    {
        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(SampleXmlDocuments.EXTERNAL_DTD)));
        assertEquals("root", d.getDocumentElement().getTagName());
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = createDocumentBuilder().parse(new InputSource(new StringReader(s)));
        assertEquals("root", d.getDocumentElement().getTagName());
        assertEquals(0, d.getDocumentElement().getChildNodes().getLength());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
