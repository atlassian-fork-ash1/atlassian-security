package com.atlassian.security.xml.libs;

import java.io.IOException;
import java.io.StringReader;

import com.atlassian.security.xml.HttpAttemptDetector;
import com.atlassian.security.xml.SampleXmlDocuments;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.JDOMParseException;
import org.jdom.input.SAXBuilder;
import org.junit.Test;

import static com.atlassian.security.xml.libs.SecureJdomFactory.newSaxBuilder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;


public class SecureJdomFactoryTest
{
    @Test(expected = JDOMException.class)
    public void settingUnknownFeaturesCausesFailure() throws JDOMException, IOException
    {
        SAXBuilder sb = new SAXBuilder();
        sb.setFeature("unknown-feature", false);

        /* An exception is only thrown when we try to parse after requesting an unknown feature */
        sb.build(new StringReader("<x/>"));
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        Document d = newSaxBuilder().build(new StringReader(SampleXmlDocuments.AMPERSAND_DOCUMENT));
        assertEquals("&", d.getRootElement().getText());
    }

    @Test(expected = JDOMParseException.class, timeout=1000)
    public void parseBillionLaughsDoesNotExhaustMemory() throws Exception
    {
        newSaxBuilder().build(new StringReader(SampleXmlDocuments.BILLION_LAUGHS));
    }

    @Test
    public void externalEntityIsNotIncludedInResult() throws Exception
    {
        Document d = newSaxBuilder().build(new StringReader(SampleXmlDocuments.externalResourceEntity()));

        assertEquals("", d.getRootElement().getText());
    }

    @Test
    public void externalEntityIsNotRead() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        newSaxBuilder().build(
                new StringReader(SampleXmlDocuments.externalResourceEntity(detector.getUrl())));

        assertFalse(detector.wasAttempted());
    }

    @Test
    public void dtdUriPointsToUrl() throws Exception
    {
        HttpAttemptDetector detector = new HttpAttemptDetector();

        new Thread(detector).start();

        String s = SampleXmlDocuments.externalUrlDtd(detector.getUrl());

        Document d = newSaxBuilder().build(new StringReader(s));
        assertEquals("root", d.getRootElement().getName());
        assertEquals(0, d.getRootElement().getContent().size());

        assertFalse("I don't want to see HTTP connection attempts", detector.wasAttempted());
    }
}
