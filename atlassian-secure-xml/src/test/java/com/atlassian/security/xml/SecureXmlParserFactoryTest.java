package com.atlassian.security.xml;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXNotRecognizedException;
import org.xml.sax.SAXNotSupportedException;

import junit.framework.Assert;

import static javax.xml.XMLConstants.FEATURE_SECURE_PROCESSING;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class SecureXmlParserFactoryTest
{
    @Test
    public void testSecureFeaturesEnabled() throws SAXException, ParserConfigurationException
    {
        SAXParserFactory factory = SecureXmlParserFactory.createSAXParserFactory();

        assertFalse(factory.getFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_GENERAL_ENTITIES));
        assertFalse(factory.getFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_PARAMETER_ENTITIES));
        assertTrue(factory.getFeature(FEATURE_SECURE_PROCESSING));
        assertFalse(factory.getFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd"));
    }

    @Test
    public void testSecureFeaturesImmutable() throws SAXException, ParserConfigurationException
    {
        SAXParserFactory factory = SecureXmlParserFactory.createSAXParserFactory();

        Assert.assertNotNull(setFeature(factory, FEATURE_SECURE_PROCESSING, false));
        Assert.assertNotNull(setFeature(factory, "http://apache.org/xml/features/nonvalidating/load-external-dtd", true));
        Assert.assertNotNull(setFeature(factory, SecureXmlParserFactory.FEATURE_EXTERNAL_GENERAL_ENTITIES, true));
        Assert.assertNotNull(setFeature(factory, SecureXmlParserFactory.FEATURE_EXTERNAL_PARAMETER_ENTITIES, true));
    }

    boolean setFeature(SAXParserFactory factory, String feature, boolean val) throws SAXNotSupportedException, SAXNotRecognizedException, ParserConfigurationException
    {
        factory.setFeature(feature, val);
        return  factory.getFeature(feature) == val;
    }


    @Test
    public void testNamespaceAwareIsNotEnabledByDefault() throws Exception
    {
        DocumentBuilder documentBuilder = SecureXmlParserFactory.newDocumentBuilder();

        String xml = "<x xmlns='a:b'/>";

        Document parse = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));

        assertNull(parse.getDocumentElement().getNamespaceURI());
    }

    @Test
    public void testNamespaceAwareIsEnabledWhenRequested() throws Exception
    {
        DocumentBuilder documentBuilder = SecureXmlParserFactory.newNamespaceAwareDocumentBuilder();

        String xml = "<x xmlns='a:b'/>";

        Document parse = documentBuilder.parse(new ByteArrayInputStream(xml.getBytes()));

        assertEquals("a:b", parse.getDocumentElement().getNamespaceURI());
    }

    @Test
    public void emptyEntityResolverIsEmpty() throws Exception
    {
        EntityResolver er = SecureXmlParserFactory.emptyEntityResolver();
        InputSource source = er.resolveEntity("anything", "anything-else");
        assertNotNull(source);
        assertEquals(-1, source.getByteStream().read());
        assertNull(source.getCharacterStream());
    }

    @Test
    public void factoryClassIsNonInstantiable() throws Exception
    {
        Class<SecureXmlParserFactory> c = SecureXmlParserFactory.class;
        assertTrue(Modifier.isFinal(c.getModifiers()));
        for (Constructor<?> cons : c.getConstructors()) {
            assertTrue(Modifier.isPrivate(cons.getModifiers()));
        }
    }

    @Test
    public void inputSourcesFromEntityResolvedAreNotRecycled() throws Exception
    {
        EntityResolver er = SecureXmlParserFactory.emptyEntityResolver();
        InputSource source = er.resolveEntity("anything", "anything-else");
        assertNotNull(source);

        InputStream dummyInputStream = new ByteArrayInputStream(new byte[0]);
        source.setByteStream(dummyInputStream);

        InputSource anotherSource = er.resolveEntity("anything", "anything-else");
        assertNotSame(dummyInputStream, anotherSource.getByteStream());
    }

    @Test
    public void DBF_FeaturesAndAttributesAreProtected() throws Exception
    {
      DocumentBuilderFactory dbf = SecureXmlParserFactory.newDocumentBuilderFactory();

      dbf.setAttribute(SecureXmlParserFactory.ATTRIBUTE_LOAD_EXTERNAL, true);
      assertTrue(Boolean.FALSE.equals(dbf.getAttribute(SecureXmlParserFactory.ATTRIBUTE_LOAD_EXTERNAL)));

      dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);
      assertTrue(dbf.getFeature(XMLConstants.FEATURE_SECURE_PROCESSING));

      dbf.setFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_GENERAL_ENTITIES, true);
      assertFalse(dbf.getFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_GENERAL_ENTITIES));

      dbf.setFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_PARAMETER_ENTITIES, true);
      assertFalse(dbf.getFeature(SecureXmlParserFactory.FEATURE_EXTERNAL_PARAMETER_ENTITIES));


    }

    @Test
    public void testDocumentBuilderFactoryIsNotNamespaceAwareByDefault() throws Exception
    {
        DocumentBuilderFactory dbf = SecureXmlParserFactory.newDocumentBuilderFactory();
        assertFalse(dbf.isNamespaceAware());

        String xml = "<x xmlns='a:b'/>";

        Document document = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));

        assertNull(document.getDocumentElement().getNamespaceURI());
    }

    @Test
    public void testDocumentBuilderFactoryIsNamespaceAwareWhenRequested() throws Exception
    {
        DocumentBuilderFactory dbf = SecureXmlParserFactory.newDocumentBuilderFactory();
        dbf.setNamespaceAware(true);
        assertTrue(dbf.isNamespaceAware());

        String xml = "<x xmlns='a:b'/>";

        Document document = dbf.newDocumentBuilder().parse(new InputSource(new StringReader(xml)));

        assertEquals("a:b", document.getDocumentElement().getNamespaceURI());
    }
}
